export const state = () => ({
  username: "eminfidan",
  message: "hello",
});

export const mutations = {
  setUsername(state, name) {
    state.username = name;
  },
};
export const actions = {
  updateUsername({ commit }, name) {
    commit("setUsername", name);
  },
};
export const getters = {
  welcomeMessage(state) {
    return `${state.message} ${state.username}`;
  },
};
